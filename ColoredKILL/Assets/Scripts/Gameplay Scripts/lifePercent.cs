﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class lifePercent : MonoBehaviour
{
    [SerializeField]
    private Image _life_slider;
    [SerializeField]
    private Text _percenttxt;
    void FixedUpdate()
    {
        _percenttxt.text = (_life_slider.fillAmount * 100).ToString() + "%";
    }
}
