﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IMovable :MonoBehaviour
{
    [SerializeField]
    protected MeshRenderer meshrenderer;
    protected bool _moving;
    protected Color _color;

    protected abstract IEnumerator Move(Vector3 dir, float Speed);
    public abstract void Dispawn();
    protected virtual void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "bullet")
        {
            IMovable _movable = col.gameObject.GetComponent<IMovable>();
            if(_movable.GetColor().Equals(_color))
                Dispawn();
        }
        if(col.gameObject.tag == "character")
            Dispawn();
    }
    public virtual void Init(Vector3 dir, float Speed, Color color)
    {
        _moving = true;
        meshrenderer.materials[0].color = color;
        _color = color;
        StartCoroutine(Move(dir, Speed));
    }
    public Color GetColor()
    {
        return _color;
    }
   
}
