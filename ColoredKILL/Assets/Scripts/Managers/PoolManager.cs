﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*script for pooling object at first loading of game :
this can instantiate all we need during the game at first instead of instantiating gameobjects on runtime 
and help and enhance the performance in runtime.
the datastructure for implementing is stack 
i get the first object from stack and when the object is to be diactivated i push the object back to the stack
 */
public class PoolManager : MonoBehaviour
{
    public enum objectType
    {
        bullet
    }
    [System.Serializable]
    public struct GameObjectStruct
    {
        public objectType type;
        public int amount;
        public GameObject prefab;
        public Stack<GameObject> pooledGameObjects;
        public int amountNow;
    }

    public GameObjectStruct[] ObjectsWeWantToPool;
    private static PoolManager _manager;
    public static PoolManager manager
    {
        get
        {
            if (_manager == null)
                _manager = GameObject.FindObjectOfType<PoolManager>();
            return _manager;
        }
    }

    void Awake()
    {
        _manager = this;
        CreatePooledObjs();

    }
    /*the structure for all Pooled Of Different GameObjects we want to Create on the Load Time,
     *  before the Main Contain of the Game Begin.*/

    public void CreatePooledObjs()
    {

        for (int i = 0; i < ObjectsWeWantToPool.Length; i++)
        {
            ObjectsWeWantToPool[i].pooledGameObjects = new Stack<GameObject>();
            ObjectsWeWantToPool[i].amountNow = ObjectsWeWantToPool[i].amount;
            //create a new GameObject for grouping each PooledGameObjects
            GameObject ParentPooledGameObject = new GameObject(ObjectsWeWantToPool[i].type + "_Parent");
            ParentPooledGameObject.transform.parent = _manager.gameObject.transform;
            for (int j = 0; j < ObjectsWeWantToPool[i].amount; j++)
            {
                Spawn(i, j, ParentPooledGameObject);
            }
        }

    }

    void Spawn(int i, int j, GameObject Parent)
    {
        var GO = Instantiate(ObjectsWeWantToPool[i].prefab);
        GO.SetActive(false);
        GO.name = ObjectsWeWantToPool[i].type + "_" + j;
        GO.transform.parent = Parent.transform;
        ObjectsWeWantToPool[i].pooledGameObjects.Push(GO);

    }
    public void pushObject(GameObject pushedObj, objectType type)
    {
        int object_index = (int)type;
        ObjectsWeWantToPool[object_index].pooledGameObjects.Push(pushedObj);
        pushedObj.transform.SetParent(manager.gameObject.transform.GetChild(object_index).transform);
        pushedObj.SetActive(false);

    }
    public GameObject GetPooledObject(objectType type)
    {
        int object_index = (int)type;
        GameObject obj = ObjectsWeWantToPool[object_index].pooledGameObjects.Pop();
        obj.SetActive(true);
        return obj;
    }
   

}
