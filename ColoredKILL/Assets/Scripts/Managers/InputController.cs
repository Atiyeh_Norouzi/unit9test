﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

    public enum InputType {  Shoot }
    public delegate void Deleg_Input(InputType type , Color color);
    public event Deleg_Input _OnClicked;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            if (_OnClicked != null)
                _OnClicked(InputType.Shoot , Color.red);
        if (Input.GetMouseButtonDown(1))
            if (_OnClicked != null)
                _OnClicked(InputType.Shoot, Color.blue);
    }
    void OnLevelWasLoaded()
    {
        _OnClicked = null;
    }
}
