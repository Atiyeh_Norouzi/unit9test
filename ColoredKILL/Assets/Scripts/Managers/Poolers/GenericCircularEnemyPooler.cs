﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericCircularEnemyPooler : MonoBehaviour
{


    #region poolObjectsStruct
    [System.Serializable]
    public struct circularEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<CircularPath_Enemy> pooledObjs;
    }
    #endregion
    public circularEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericCircularEnemyPooler _instance;
    public static GenericCircularEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericCircularEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<CircularPath_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            CircularPath_Enemy c = GO.GetComponent<CircularPath_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }


    public void pushEnemy(CircularPath_Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(enemy);
        enemy.gameObject.transform.SetParent(enemyParent.transform);
        enemy.gameObject.transform.localPosition = Vector3.zero;
    }


    public CircularPath_Enemy getEnemy()
    {
        CircularPath_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }


}
