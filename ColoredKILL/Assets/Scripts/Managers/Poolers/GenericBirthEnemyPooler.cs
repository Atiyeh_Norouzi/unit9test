﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericBirthEnemyPooler : MonoBehaviour {


    #region poolObjectsStruct
    [System.Serializable]
    public struct BirthEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<GenerateChilds_Enemy> pooledObjs;
    }
    #endregion
    public BirthEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericBirthEnemyPooler _instance;
    public static GenericBirthEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericBirthEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<GenerateChilds_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            GenerateChilds_Enemy c = GO.GetComponent<GenerateChilds_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }
  

    public void pushEnemy(GenerateChilds_Enemy birthEnemy)
    {
        birthEnemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(birthEnemy);
        birthEnemy.gameObject.transform.SetParent(enemyParent.transform);
        birthEnemy.gameObject.transform.localPosition = Vector3.zero;
    }
  

    public GenerateChilds_Enemy getEnemy()
    {
        GenerateChilds_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }
   

}
